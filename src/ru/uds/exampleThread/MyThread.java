package ru.uds.exampleThread;

public class MyThread extends Thread {
    MyThread(String name) {
        super(name);
    }

    public void run() {
        System.out.println(getName() + " запущен");
    }
}