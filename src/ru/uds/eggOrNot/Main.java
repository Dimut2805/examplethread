package ru.uds.eggOrNot;

public class Main {
    public static void main(String[] args) {
        Thread egg = new MyThread("Яйцо");
        Thread chicken = new MyThread("Курица");
        egg.start();
        chicken.start();
        try {
            if (egg.isAlive()) {
                chicken.join();
                System.out.println("Курица победила");
            } else if (chicken.isAlive()) {
                egg.join();
                System.out.println("Яйцо победило");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}