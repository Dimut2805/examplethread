package ru.uds.sportAnimals;

public class RabbitAndTurtle {
    public static void main(String[] args) {
        AnimalThread turtle = new AnimalThread("turtle", 5);
        AnimalThread rabbit = new AnimalThread("rabbit", 10);
        turtle.start();
        rabbit.start();
    }
}