package ru.uds.sportAnimals;

public class AnimalThread extends Thread {
    private String name;

    AnimalThread(String name, int priority) {
        this.name = name;
        setName(name);
        setPriority(priority);
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i == 50 && name.equals("rabbit")) {
                setPriority(MIN_PRIORITY);
            }
            if (i == 50 && name.equals("turtle")) {
                setPriority(MAX_PRIORITY);
            }
            System.out.println(name + " " + i);
        }
    }
}